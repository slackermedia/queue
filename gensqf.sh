#!/bin/bash
ARG="${1}"


function help() {
    echo "Generate queue files for Slackermedia. Syntax:" 
    echo "gensqf.sh TEMPLATE (probably slackermedia-master.sqf)"
    echo " "
    echo "-h, --help    Print this help and exit."
    echo "-r, --release Git branch, tag, and push release."
    echo "-c, --clean   Remove builds from working directory."
}

function release() {
    COM=`git rev-list --tags --max-count=1`
    FUL=`git describe --tags $COM`
    MIN=`echo $FUL | rev | cut -b1`
    ADD=`echo $MIN + 1 | bc`
    MAX=`echo $FUL | cut -f -2 -d"."`
    REL=`echo "$MAX"."$ADD"`
    
    git add slackermedia-{a,v,g}.sqf slackermedia-huge.sqf
    git commit -m "release ${REL}"
    git tag -a "${REL}" -m "release ${REL}"
    echo "Check things over. If everything is OK, push to git repo:"
    echo "  $ git push origin HEAD"
    echo "  $ git push --tags"
}

function clean() {
    /usr/bin/rm slackermedia-?.sqf
    /usr/bin/rm slackermedia-huge.sqf
}

function main() {
    for SET in a v g
    do
	#echo ":${SET}"
	for LINE in $( < ${ARG} )
	do
	    echo ${LINE} | grep ":${SET}" | cut -f1 -d":" >> slackermedia-"${SET}".sqf
	    echo ${LINE} | grep ":x" | cut -f1 -d":" >> slackermedia-"${SET}".sqf
	done
    done

    # generate huge
    cat ${ARG} | cut -f1 -d":" >> slackermedia-huge.sqf
}


if [ -z "${1}" ]; then
    help
    exit
elif [ "${1}" == "-h" -o "${1}" == "--help" -o "${1}" == "help" ]; then
    help
    exit
fi

if [ "${1}" == "-r" -o "${1}" == "--release" -o "${1}" == "release" ]; then
    main
    release
elif [ "${1}" == "-c" -o "${1}" == "--clean" -o "${1}" == "clean" ]; then
    clean
else
    main
fi

#echo "you chose ${ARG}"

