# Slackermedia Queue Files

This git repo tracks changes to the Slackermedia dependency queue file.

Queue files generated from this are compatible with [sbopkg](http://sbopkg.org), [Sport](http://slackermedia.info/sport), and others.

See http://slackermedia.info/handbook for more information.